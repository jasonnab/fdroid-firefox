Unofficial F-Droid repo for various apps
========================================

Currently included (alphabetically):

* Firefox (may sometimes be a release candidate)
* Firefox Beta
* Signal

Add this URL to F-Droid:
https://xxxx.gitlab.io/fdroid-firefox/fdroid/repo?fingerprint=

[![Repo URL QRcode](fdroid/public/repo-qrcode.png)](https://xxxx.gitlab.io/fdroid-firefox/fdroid/repo?fingerprint=)

For some apps, this repo may include APKs of more than one architecture that is compatible with your device, and all of them will appear in the "Versions" list in the F-Droid client.
To show the architecture of each APK, enable "Expert mode" in the F-Droid settings.

All code and other work in this repository is in the public domain, i.e. licensed as such.<br />
**Both, this Gitlab repository and this unofficial F-Droid repository
are not affiliated to, nor have been authorized, sponsored or otherwise approved by the app developers.**

You can see what's inside the repo here: https://xxxx.gitlab.io/fdroid-firefox/fdroid/repo/index.xml


How does it work?
=================

This Gitlab repository contains the complete source code to configure Gitlab CI/CD and this F-Droid repository.

The private key for signing this unofficial F-Droid repository is kept private, but only used for exactly this purpose.
To generate your own key run:
```
keytool -genkey -v -keystore my.keystore -alias repokey -keyalg RSA -keysize 2048 -validity 10000 -storepass passw0rd1
```

Get base64 representation (ASCII only characters) of the certificate:
```
base64 my.keystore
```
Then paste it into to the gitlab variables.

**The APKs are unaltered and hence still signed by the app developers.**

How to include new apps?
========================

Click [here](ADDAPPS.md) to see how to add a new app to this repo.

Please note that the repo has to be kept small because there's a 1 GB limit on Gitlab pages.
If you need specific apps, you can fork this repo and maintain your own one.

See [LICENSE.md](LICENSE) for license info. Please sign your commit
(git -s) to accept the license.

How to generate repo metadata
=============================
Make sure a Java SDK is installed first (or install jarsigner/apksigner separately)

* Install fdroidserver, as outlined here: https://f-droid.org/en/docs/Installing_the_Server_and_Repo_Tools
* after installing: `cd ./fdroid`
* Edit config.yml as desired (follow this guide for general suggestions: https://android.izzysoft.de/articles/named/fdroid-simple-binary-repo?lang=en )
* `fdroid update --create-key`
* If successful, `fdroid update -c`
* Setup your web server as desired, point to the repo folder
    * nginx example:

```
        location /public/fdroid-firefox/fdroid/repo {
                autoindex off;
                alias /var/www/fdroid-firefox/fdroid/repo;
        }

```

Donations
=========

If you want to donate, please donate to the respective app developers instead!
